﻿namespace Orchard.Cms.Core.Contracts
{
    public interface IMemoryManager
    {
        object Get(object key);
        void Set(object key,object value);
    }
}
