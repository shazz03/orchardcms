﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Orchard.Cms.Core.Model;

namespace Orchard.Cms.Core.Contracts
{
    public interface IPost
    {
        Task<List<Post>> GetByUserId(int? id);
        void AddPost(Post post);
    }
}
