﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Orchard.Cms.Core.Model;

namespace Orchard.Cms.Core.Contracts
{
    public interface IServiceHandler
    {
        /// <summary>
        /// Api Url
        /// </summary>
        Uri ApiUrl { get; set; }
        /// <summary>
        /// HttpMethod
        /// </summary>
        HttpMethodOptions Method { get; set; }
        /// <summary>
        /// Http Headers
        /// </summary>
        Dictionary<string, string> Headers { get; set; }
        /// <summary>
        /// Calling web service Asynchrnously
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <typeparam name="TRequest"></typeparam>
        /// <param name="resource"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<TResponse> Call<TResponse, TRequest>(string resource, TRequest request);
    }
}
