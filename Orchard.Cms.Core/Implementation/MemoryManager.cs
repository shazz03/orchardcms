﻿using System.Collections.Generic;
using Orchard.Cms.Core.Contracts;

namespace Orchard.Cms.Core.Implementation
{
    public class MemoryManager : IMemoryManager
    {
        /// <summary>
        /// Get value from memory
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object Get(object key)
        {
            object value;
            MemoryWrapper.Memory.TryGetValue(key, out value);
            return value;
        }

        /// <summary>
        /// Set value in memory
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Set(object key, object value)
        {
            if (Get(key) == null)
            {
                MemoryWrapper.Memory.Add(key, value);
            }

            else
            {
                MemoryWrapper.Memory[key] = value;
            }
        }
    }
}
