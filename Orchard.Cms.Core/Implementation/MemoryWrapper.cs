﻿using System.Collections.Generic;

namespace Orchard.Cms.Core.Implementation
{
    public static class MemoryWrapper
    {
        private static Dictionary<object,object> _memory;

        public static Dictionary<object, object> Memory => _memory ?? (_memory = new Dictionary<object, object>());
    }
}
