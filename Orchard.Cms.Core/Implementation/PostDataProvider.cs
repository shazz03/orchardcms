﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Orchard.Cms.Core.Contracts;
using Orchard.Cms.Core.Model;
using Orchard.Cms.Core.Utilities;

namespace Orchard.Cms.Core.Implementation
{
    public class PostDataProvider : IPost
    {
        private readonly IServiceHandler _serviceHandler;
        private readonly IMemoryManager _memoryManager;
        public PostDataProvider(IServiceHandler serviceHandler, IMemoryManager memoryManager)
        {
            _serviceHandler = serviceHandler;
            _memoryManager = memoryManager;
            _serviceHandler.ApiUrl = Utility.JsonPlaceHolderBaseUri;
        }

        public async Task<List<Post>> GetByUserId(int? id)
        {
            _serviceHandler.Method = HttpMethodOptions.Get;
            return await _serviceHandler.Call<List<Post>, string>($"/users/{id}/posts", string.Empty);
        }

        public void AddPost(Post post)
        {
            var allPosts = new List<Post>();

            var getPosts = (List<Post>)_memoryManager.Get("posts");
            if (getPosts != null)
            {
                allPosts.AddRange(getPosts);
            }
            post.Id = allPosts.Count + 1;
            allPosts.Add(post);
            _memoryManager.Set("posts", allPosts);
        }
    }
}

