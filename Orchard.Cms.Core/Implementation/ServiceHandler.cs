﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Orchard.Cms.Core.Contracts;
using Orchard.Cms.Core.Model;

namespace Orchard.Cms.Core.Implementation
{
    public class ServiceHandler : IServiceHandler
    {
        public Uri ApiUrl { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public HttpMethodOptions Method { get; set; }
        public async Task<TResponse> Call<TResponse, TRequest>(string resource, TRequest request)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = ApiUrl;
                if (Method == HttpMethodOptions.Get)
                {
                    var response = await httpClient.GetStringAsync(resource);
                    return JsonConvert.DeserializeObject<TResponse>(response);
                }
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.Timeout = TimeSpan.FromMinutes(2);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var jsonBody = JsonConvert.SerializeObject(request);
                var httpRequest = new StringContent(jsonBody);
                httpRequest.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                if (Headers != null)
                {
                    foreach (var header in Headers)
                    {
                        httpRequest.Headers.Add(header.Key, header.Value);
                    }
                }
                //call web service asynchronously 
                using (var response = await httpClient.PostAsync(resource, httpRequest))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        //converting the response into generic object supplied from origional request
                        using (var content = response.Content)
                        {
                            var responseJson = await content.ReadAsStringAsync();
                            return JsonConvert.DeserializeObject<TResponse>(responseJson);
                        }
                    }
                    throw new Exception(response.StatusCode.ToString());
                }
            }
        }
    }
}
