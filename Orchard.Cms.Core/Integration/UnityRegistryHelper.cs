﻿using Microsoft.Practices.Unity;
using Orchard.Cms.Core.Contracts;
using Orchard.Cms.Core.Implementation;

namespace Orchard.Cms.Core.Integration
{
    public static class UnityRegistryHelper 
    {
         
        public static UnityContainer RegisterContainer()
        {
            var container = new UnityContainer();
            container.RegisterType<IServiceHandler, ServiceHandler>();
            container.RegisterType<IPost, PostDataProvider>();

            return container;
        }
    }
}
