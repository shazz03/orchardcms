﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orchard.Cms.Core.Model
{
    public enum HttpMethodOptions
    {
        Get,
        Post
    }
}
