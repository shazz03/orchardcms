﻿using System;

namespace Orchard.Cms.Core.Utilities
{
    public static class Utility
    {
        public static Uri JsonPlaceHolderBaseUri => new Uri("https://jsonplaceholder.typicode.com/");
    }
}
