﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Orchard.Cms.Core.Contracts;
using Orchard.Cms.Web;
using Orchard.Cms.Web.Controllers;

namespace Orchard.Cms.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        private Mock<IServiceHandler> _servicMock;
        public void Init()
        {
            _servicMock=new Mock<IServiceHandler>();
        }
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController(_servicMock.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void MyProfile()
        {
            // Arrange
            HomeController controller = new HomeController(_servicMock.Object);

            // Act
            ViewResult result = controller.MyProfile() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Dasshboard()
        {
            // Arrange
            HomeController controller = new HomeController(_servicMock.Object);

            // Act
            ViewResult result = controller.Dashboard(1).Result as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController(_servicMock.Object);

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
