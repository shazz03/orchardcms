﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orchard.Cms.Core.Implementation;
using Orchard.Cms.Core.Model;

namespace Orchard.Cms.Tests
{
    [TestClass]
    public class MemoryManagerTests
    {
        private MemoryManager _memoryManager;
        private List<Post> _listOfPosts;

        [TestInitialize]
        public void Init()
        {
            _memoryManager = new MemoryManager();
            _listOfPosts = new List<Post>
            {
                new Post() {Body = "Hello I am a post1.", Title = "Post title1"},
                new Post() {Body = "Hello I am a post2.", Title = "Post title2"}
            };
        }

        public void SetMemory()
        {
            _memoryManager.Set("MyKey", _listOfPosts);
        }

        [TestMethod]
        public void MemoryManager_Get_Should_Return_Null()
        {
            var value = _memoryManager.Get("MyKey");
            Assert.AreEqual(null, value);
        }

        [TestMethod]
        public void MemoryManager_Set_Value()
        {
            _memoryManager.Set("MyKey", _listOfPosts);
            var value = (List<Post>)_memoryManager.Get("MyKey");
            Assert.AreEqual("Post title1", value.FirstOrDefault().Title);
        }

        [TestMethod]
        public void MemoryManager_Get_Should_Return_ListOfPost(string title)
        {
            SetMemory();
            var value = (List<Post>)_memoryManager.Get("MyKey");
            Assert.AreEqual("Post title1", value.FirstOrDefault().Title);
        }
    }
}
