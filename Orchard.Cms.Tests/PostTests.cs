﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Orchard.Cms.Core.Contracts;
using Orchard.Cms.Core.Model;

namespace Orchard.Cms.Tests
{
    [TestClass]
    public class PostTests
    {
        private Mock<IPost> _postMock;
        private List<Post> _listOfPosts;


        [TestInitialize]
        public void Init()
        {
            _postMock = new Mock<IPost>();
            _listOfPosts = new List<Post>
            {
                new Post() {Body = "Hello I am a post1.", Title = "Post title1"},
                new Post() {Body = "Hello I am a post2.", Title = "Post title2"}
            };
        }

        [TestMethod]
        public void Post_Should_Return_Posts()
        {
            
            _postMock.Setup(x => x.GetByUserId(1)).ReturnsAsync(_listOfPosts);
          
            Assert.AreEqual(2, _listOfPosts.Count);
        }
    }
}
