﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Orchard.Cms.Core.Contracts;
using Orchard.Cms.Core.Model;
using Orchard.Cms.Core.Utilities;
using Orchard.Cms.Web.Filters;

namespace Orchard.Cms.Web.Controllers
{
    [Authentication]
    public class HomeController : Controller
    {
        private readonly IServiceHandler _serviceHandler;

        public HomeController(IServiceHandler serviceHandler)
        {
            _serviceHandler = serviceHandler;
            _serviceHandler.ApiUrl = Utility.JsonPlaceHolderBaseUri;
        }

        [Authentication(true)]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Dashboard");
            }
            return View();
        }
        [RequestFilter(1)]
        public async Task<ActionResult> Dashboard(int? id)
        {
            _serviceHandler.Method = HttpMethodOptions.Get;
            var user = await _serviceHandler.Call<User, string>($"/users/{id}", string.Empty);
            return View(user);
        }

        public ActionResult MyProfile()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}