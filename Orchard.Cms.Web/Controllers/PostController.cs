﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Orchard.Cms.Core.Contracts;
using Orchard.Cms.Core.Model;
using Orchard.Cms.Core.Utilities;
using Orchard.Cms.Web.Filters;

namespace Orchard.Cms.Web.Controllers
{
    public class PostController : ApiController
    {
        private readonly IPost _post;
        private readonly IMemoryManager _memoryManager;
        public PostController(IPost post, IMemoryManager memoryManager)
        {
            _post = post;
            _memoryManager = memoryManager;
        }

        /// <summary>
        /// Get All posts by user id 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [Route("post/get/{id}")]
        [HttpGet]
        public async Task<List<Post>> Get(int? id, int pageSize, int pageIndex)
        {
            var memoryFeed = (List<Post>)_memoryManager.Get("posts");
            if (memoryFeed == null)
            {
                var post = await _post.GetByUserId(id);
                if (post != null)
                {
                    _memoryManager.Set("posts", post);
                }
            }
            memoryFeed = (from p in (List<Post>)_memoryManager.Get("posts")
                          orderby p.Id descending
                          select p).Page(pageSize, pageIndex).ToList();

            return memoryFeed;
        }

        [HttpPost]
        [Authorization]
        [RequestFilter(1)]
        [Route("post/add")]
        public void Add(Post post)
        {
            _post.AddPost(post);

            Request.CreateResponse(
                HttpStatusCode.OK, "Saved!", ControllerContext.Configuration.Formatters.JsonFormatter);
        }
    }
}
