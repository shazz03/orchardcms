﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace Orchard.Cms.Web.Filters
{
    public class AuthenticationAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        public bool Ignore { get; }

        public AuthenticationAttribute()
        {
            Ignore = false;
        }

        public AuthenticationAttribute(bool ignore)
        {
            Ignore = ignore;
        }
        public void OnAuthentication(AuthenticationContext filterContext)
        {

        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (Ignore) return;
            //redirect to login page if user is not logged in
            var user = filterContext.HttpContext.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }
    }
}