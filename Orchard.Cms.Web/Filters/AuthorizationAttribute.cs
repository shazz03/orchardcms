﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Orchard.Cms.Web.Filters
{
    public class AuthorizationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string message;
            var appKey = "Bearer T3JjaGFyZEFwaUtleQ==";
            try
            {
                var requestHeaders = actionContext.Request.Headers;
                if (requestHeaders != null)
                {
                    IEnumerable<string> values;
                    if (actionContext.Request.Headers.TryGetValues("Authorization", out values))
                    {
                        if (appKey == values.FirstOrDefault())
                            return;
                        //Unable to verify app key with requested app key Authentication failed
                        message = "Authentication failed!";
                    }
                    else
                    {
                        message = "Invalid authentication header.";
                    }
                }
                else
                {
                    message = "Invalid authentication header.";
                }
            }
            catch (Exception ex)
            {
                message = $"Exception:{ex.Message}";
            }
            actionContext.Response = actionContext.Request.CreateResponse(
                HttpStatusCode.Unauthorized, message, actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            try
            {

            }
            catch (Exception)
            {

                actionExecutedContext.Request.CreateResponse(HttpStatusCode.BadRequest, "Bad request");
            }
        }
    }
}