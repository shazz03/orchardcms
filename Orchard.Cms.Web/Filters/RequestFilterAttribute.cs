﻿using System.Web.Mvc;

namespace Orchard.Cms.Web.Filters
{
    public class RequestFilterAttribute : ActionFilterAttribute
    {
        public RequestFilterAttribute(int id)
        {
            Id = id;
        }

        private int Id { get; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.ActionParameters.ContainsKey("id"))
            {
                filterContext.ActionParameters["id"] = Id;
            }
        }
    }
}