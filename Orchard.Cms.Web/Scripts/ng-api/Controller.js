﻿app.controller('apiController', function ($scope, apiService,$timeout) {
    $scope.postStatus = "Most Recent";
    var id = $("#tblPosts").data("id");
    var pageIndex = parseInt($("#tblPosts").data("pageindex"));
    getAll(id, 5, pageIndex);
    $scope.loading = true;
    $scope.postsection = false;
    function getAll(id, pageSize, pageIndex) {
        var servCall = apiService.getPosts(id, pageSize, pageIndex);
        servCall.then(function (d) {
            $scope.posts = d.data;
            if (d.data.length === 0) {
                $("#btnOldest").hide();
            } else {
                $("#btnOldest").show();
            }
        },
            function (error) {
                alert('Oops! Something went wrong while fetching the data.');
            }).finally(function () {
            showLoader();
        });
    }

    function showLoader() {
        $timeout(function () {
            $scope.loading = false;
            $scope.postsection = true;
        }, 3000);
    }

    $scope.getPosts = function (isLatest) {
        $scope.loading = true;
        $scope.postsection = false;
        showLoader($scope);
        var newIndex = parseInt($("#tblPosts").data("pageindex")) + 1;

        if (isLatest) {
            newIndex = 0;
            $scope.postStatus = "Most Recent";
        } else {
            $scope.postStatus = "Previous Posts";
        }
        getAll(id, 5, newIndex);
        $("#tblPosts").data("pageindex", newIndex);
    };

    $scope.savePost = function () {
        var post = {
            Title: $scope.title,
            Body: $scope.body
        };

        var savePost = apiService.savePost(post);
        savePost.then(function(d) {
                getAll(id, 5, 0);
            },
            function(error) {
                console.log('Oops! Something went wrong while saving the data.');
            });
    };
   
});
