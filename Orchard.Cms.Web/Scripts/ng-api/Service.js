﻿app.service("apiService", function ($http) {
    this.getPosts = function (id, pageSize, pageIndex) {
        return $http.get("../../post/get/" + id + "?pageSize=" + pageSize + "&pageIndex=" + pageIndex);
    }

    this.savePost = function (post) {
        $http.defaults.headers.common['Authorization'] = "Bearer T3JjaGFyZEFwaUtleQ==";
        return $http(
            {
                method: 'post',
                data: post,
                url: '../../post/add'
            });
    }
});