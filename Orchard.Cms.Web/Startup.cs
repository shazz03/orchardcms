﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Orchard.Cms.Web.Startup))]
namespace Orchard.Cms.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
